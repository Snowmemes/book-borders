#!/bin/luajit

local lfs = require "lfs"
local exec = os.execute

local flags = {
	"-fsanitize=address",
	"-std=c++11",
	"-lm",

	"-Wall",
	"-Wextra",
	--"-Werror",

	"-Wshadow",
	"-Wpointer-arith",
	"-Wstrict-overflow=5",
	"-Wswitch-default",
	"-Wundef",
	"-Wcast-align",
	"-Wconversion",
	"-Wfloat-equal",
	"-Waggregate-return",
	"-Wcast-qual",
	"-Wswitch-enum",
	"-Wunreachable-code",
	"-Wwrite-strings",
	"-Winit-self",
	"-Wformat=2",
	"-Wno-long-long",

	"-march=native",
	"-O2",
	"-pedantic"
}

local filter = ( { ... } )[ 1 ] or ""

local code = exec( "g++ " .. table.concat( flags, " " ) .. " main.cpp -o exe" )

if code ~= 0 then
	error( "Compilation failed." )
end

local sample_path = "./tests/"
local passed = 0
local total = 0

for file in lfs.dir( sample_path ) do
	if file:find( ".in" ) and file:find( filter ) then
		print( "Testing " .. file )

		local in_file = io.open( sample_path .. file, "r" )
		local input = in_file:read( "*a" )
		in_file:close()

		local expected_result_path = sample_path .. file:gsub( "in", "out" )
		local out_file = io.open( expected_result_path, "r" )
		local output = out_file:read( "*a" )
		out_file:close()

		local f = io.popen( "./exe > program_output.txt", "w" )
		f:write( input )
		f:close()

		f = io.open( "program_output.txt", "r" )
		local actual_output = f:read( "*a" )
		f:close()

		if actual_output ~= output then
			print( "  Input:" )
			exec( "cat " .. sample_path .. file )
			print( "  Failed" )
			--print( "  output mismatch\n  expected:" .. output .. "\n  actual:" .. actual_output )
			print( "  output mismatch" )
			exec( "diff --color=always --text --label='diff' -c '"
				.. expected_result_path .. "' program_output.txt" )
		else
			print( "  Passed" )
			passed = passed + 1
		end

		total = total + 1
	end
end

print( "\27[32m" .. string.rep( "●", passed ) .. "\27[31m" .. string.rep( "●", total - passed ) .. "\27[0m" )

print( "Passed " .. passed .. " out of " .. total .. " tests." )

