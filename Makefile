
CXX = g++
CXX_FLAGS = -Wall -Wextra -pedantic -Wno-long-long -fsanitize=address

exe: main.cpp
	$(CXX) $(CXX_FLAGS) main.cpp -o exe

.PHONY: test

test: exe
	@./test.lua
